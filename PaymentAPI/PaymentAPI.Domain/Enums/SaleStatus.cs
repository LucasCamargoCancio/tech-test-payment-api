﻿using System.ComponentModel;

namespace PaymentAPI.Domain.Enums
{
    public enum SaleStatus
    {
        [Description("Aguardando pagamento")]
        AwaitingPayment,
        [Description("Pagamento Aprovado")]
        Approved,
        [Description("Cancelada")]
        Canceled,
        [Description("Enviado para Transportadora")]
        SentToCarrier,
        [Description("Entregue")]
        Delivered
    }
}
