﻿namespace PaymentAPI.Domain.Entities
{
    public class Order : BaseEntity
    {
        public int Quantity { get; private set; }
        public Product Product { get; private set; }
    }
}
