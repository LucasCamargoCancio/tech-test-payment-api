﻿using PaymentAPI.Domain.Enums;

namespace PaymentAPI.Domain.Entities
{
    public class SaleHistory : BaseEntity
    {
        public Sale Sale { get; private set; }
        public SaleStatus Status { get; private set; }
    }
}
