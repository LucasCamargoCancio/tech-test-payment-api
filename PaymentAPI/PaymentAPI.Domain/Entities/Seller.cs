﻿namespace PaymentAPI.Domain.Entities
{
    public class Seller : BaseEntity
    {
        public string Cpf { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
    }
}
