﻿namespace PaymentAPI.Domain.Entities
{
    public class Sale : BaseEntity
    {
        public List<Order> Orders { get; private set; }
        public Seller Seller { get; private set; }
    }
}
