﻿using PaymentAPI.Domain.Entities;

namespace PaymentAPI.Domain.Interfaces.Repositories
{
    internal interface ISellerRepository
    {
        Task<List<Seller>> GetAll();
        Task<Seller> GetById(Guid id);
        Task Update(Seller seller);
        Task<Seller> Create(Seller seller);
        Task DeleteById(Guid id);
    }
}
