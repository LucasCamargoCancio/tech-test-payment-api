﻿using PaymentAPI.Domain.Entities;

namespace PaymentAPI.Domain.Interfaces.Repositories
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAll();
        Task<Product> GetById(Guid id);
        Task Update(Product product);
        Task<Product> Create(Product product);
        Task DeleteById(Guid id);
    }
}
