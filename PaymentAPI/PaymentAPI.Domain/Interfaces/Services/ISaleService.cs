﻿using PaymentAPI.Domain.Entities;

namespace PaymentAPI.Domain.Interfaces.Services
{
    internal interface ISaleService
    {
        Task Sell();
        Task UpdateSale();
        Task<Sale> SearchSaleById(Guid id);
    }
}
