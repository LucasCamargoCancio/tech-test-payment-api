﻿using PaymentAPI.Domain.Entities;

namespace PaymentAPI.Domain.Interfaces.Services
{
    public interface ISellerRepository
    {
        Task Create(Seller seller);
        Task Update(Seller seller);
    }
}
