﻿using Microsoft.EntityFrameworkCore;
using PaymentAPI.Domain.Entities;

namespace PaymentAPI.Infrastructure
{
    public class PaymentAPIContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleHistory> SaleHistories { get; set; }
        public DbSet<Seller> Sellers { get; set; }

        public PaymentAPIContext(DbContextOptions<PaymentAPIContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
