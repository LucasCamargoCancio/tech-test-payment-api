using Microsoft.Extensions.Configuration;
using PaymentAPI.Infrastructure;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<PaymentAPIContext>(opts =>
{
    var connString = builder.Configuration.GetConnectionString("PaymentAPIConnection");
    opts.UseSqlServer(connString, options =>
    {
        var assemblyName = typeof(PaymentAPIContext).Assembly.FullName.Split(',')[0];
        options.MigrationsAssembly(assemblyName);
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
